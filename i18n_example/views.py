from django.shortcuts import render
from django.utils.translation import ugettext_lazy as _


def home(request, template="home.html"):
    return render(
        request,
        template,
        dict(
            foo = _("A string from the view"),
            bar = "Another string form the view",
        )
    )
