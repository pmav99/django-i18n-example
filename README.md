i18n Example
------------

A companion project to my blog post about i18n: http://albertoconnor.ca/blog/2013/Aug/19/django-i18n

To work through this example first clone the project.

The setup is pretty standard, assuming you have created a virtualenv

    pip install -r requirements.txt
    python manage.py syncdb
    python manage.py runserver

You will now see the untranslated main page, no matter what language code you provide.

The project has already been setup to use translations, and has some strings already marked for translation.

Now you can follow the instructions in the blog post to create a german translation is fake translastions to help you determine coverage.

    mkdir locale
    python manage.py makemessages -l de
    poxx.py locale/de/LC_MESSAGES/django.po
    python manage.py compilemessages
    python manage.py runserver

Now you should be able to see fake translations at http://127.0.0.1:8000/de/. You can also checkout the builtin translations to the admin, http://127.0.0.1:8000/de/admin/.

Notes
-----
A few things are already done for you but you should be aware of them:

* LOCALE_PATH was set in settings.py
* i18n_patterns was used in urls.py
* ugettext was imported and used in views.py
* {% load i18n %} and the {% trans 'foo' %} templates tags were used in base.html and home.html
